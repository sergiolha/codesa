CREATE USER codesa IDENTIFIED BY "codesa";
GRANT CONNECT, resource TO codesa;
ALTER USER codesa quota unlimited ON USERS;
CREATE TABLE codesa.rol (
	id_rol number(19,0) NOT NULL,
	nombre varchar2(150) NOT NULL,
	PRIMARY KEY (id_rol)
);
CREATE TABLE codesa.usuario (
	id_usuario number(19,0) NOT NULL,
	id_rol number(19,0) NOT NULL,
	nombre varchar2(150) NOT NULL,
	activo char NOT NULL,
	PRIMARY KEY (id_usuario)
);
ALTER TABLE CODESA.USUARIO ADD CONSTRAINT FK_ROL FOREIGN KEY (ID_ROL) REFERENCES CODESA.ROL(ID_ROL);
CREATE SEQUENCE codesa.seq_usuario MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 nocache nocycle;
CREATE OR REPLACE TRIGGER codesa.ti_usuario
BEFORE INSERT ON codesa.usuario
REFERENCING NEW AS NEW FOR EACH ROW 
DECLARE l_conseq NUMBER(19,0) := 0;
BEGIN 
	SELECT codesa.seq_usuario.nextval INTO l_conseq FROM dual;
	:NEW.id_usuario := l_conseq;
END;
GRANT SELECT, INSERT, UPDATE, DELETE ON rol TO codesa;
GRANT SELECT, INSERT, UPDATE, DELETE ON usuario TO codesa;
INSERT INTO rol VALUES (1,'ADMINISTRADOR');
INSERT INTO rol VALUES (2,'AUDITOR');
INSERT INTO rol VALUES (3,'AUXILIAR');

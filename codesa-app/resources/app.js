$(document).ready(function() {

    var roles = [];
    var usuarios = [];

    function rolSelect(roles) {
        var rolesHTML = roles.map((rol) => `<option value="${rol.id}">${rol.nombre}</option>`);
        return `<select class="form-select" aria-label="Instructor" id="rol" required>
        <option value="">Seleccione rol</option>
        ${rolesHTML}
        </select>
        <div class="invalid-feedback">
        Por favor seleccione un rol.
        </div>`;
    }

    function usuarioTabla(usuarios) {
        var activoOpt = { 'S': 'Si', 'N': 'No' };
        var usuariosHTML = usuarios.map((usuario) => `<tr data-id="${usuario.id}" name="usuariosTR"><th scope="row">${usuario.id}</th>
                                            <td>${usuario.nombre}</td>
                                            <td>${roles.filter(r => r.id === usuario.rol_id)[0].nombre}</td>
                                            <td>${activoOpt[usuario.activo]}</td></tr>`);
        return usuariosHTML;
    }

    function accionUsuario(event, method) {
        var usuarioId = $('#_userId').val();
        var form = $('.needs-validation')[0];
        if (!form.checkValidity()) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            var pathURL = 'http://localhost:8080/usuarios';
            if (usuarioId !== '') {
                pathURL = `http://localhost:8080/usuarios/${usuarioId}`
            }
            var sendData = {};
            switch (method) {
                case 'POST':
                case 'PUT':
                    sendData = {
                        nombre: $('#nombre').val(),
                        rol_id: $('#rol').val(),
                        activo: $('input[name="activo"]:checked').val()
                    }
                    break;
                case 'DELETE':
                    break;
                default:
                    alert('Método nulo no permitido');
                    form.classList.add('was-validated');
                    return;
            }
            form.classList.add('was-validated');
            $.ajax({
                crossDomain: true,
                type: method,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                url: pathURL,
                data: JSON.stringify(sendData)
            }).then(function() {
                $('#formUsuario').trigger("reset");
                $('#guardarBtn').attr("disabled", "disabled");
                $('#editarBtn').attr("disabled", "disabled");
                $('#eliminarBtn').attr("disabled", "disabled");
                $('input[name="activo"]').removeAttr("checked");
                $('#_userId').val('');
            }).fail(function(err) {
                alert('Error con la petición. ' + err.responseJSON.description);
            }).always(function() {
                $('#busqueda').val('');
                form.classList.remove('was-validated');
                listarUsuarios();
            });
        }
    }

    function listarUsuarios() {
        var busqueda = $('#busqueda').val();
        $.ajax({
            crossDomain: true,
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:8080/usuarios",
            data: { busqueda: busqueda }
        }).then(function(data) {
            if (data._embedded === undefined) {
                $('#usuariosList').html("<tr><td colspan='4'>Sin registros</td></tr>");
                usuarios = [];
                return;
            }
            usuarios = data._embedded.usuarios;
            tabla = usuarioTabla(usuarios);
            $('#usuariosList').html(tabla);
            $('tr[name="usuariosTR"]').on('click', function() {
                var id = $(this).data('id');
                var data = usuarios.filter(u => u.id === id)[0];
                $('#formUsuario').trigger("reset");
                $('#guardarBtn').attr("disabled", "disabled");
                $('#editarBtn').removeAttr("disabled");
                $('#eliminarBtn').removeAttr("disabled");
                $('#_userId').val(data.id);
                $('#idUsuario').val(data.id);
                $('#nombre').val(data.nombre);
                $('#rol').val(data.rol_id);
                $('input[name="activo"][value="' + data.activo + '"]').attr("checked", "checked");
            });
        });
    }

    $.ajax({
        crossDomain: true,
        dataType: "json",
        contentType: "application/json",
        url: "http://localhost:8080/roles"
    }).then(function(data) {
        if (data._embedded === undefined) {
            $('#rolList').html("-- Sin roles --");
            roles = [];
            return;
        }
        roles = data._embedded.rols;
        select = rolSelect(roles);
        $('#rolesList').html(select);
        listarUsuarios();
    });

    $('#consultarBtn').on('click', listarUsuarios);

    $('#limpiarBtn').on('click', function() {
        $('#busqueda').val('');
        $('#formUsuario').trigger("reset");
        $('#guardarBtn').attr("disabled", "disabled");
        $('#editarBtn').attr("disabled", "disabled");
        $('#eliminarBtn').attr("disabled", "disabled");
        $('input[name="activo"]').removeAttr("checked");
        $('#_userId').val('');
    });

    $('#crearBtn').on('click', function() {
        $('#formUsuario').trigger("reset");
        $('#guardarBtn').removeAttr("disabled");
        $('#editarBtn').attr("disabled", "disabled");
        $('#eliminarBtn').attr("disabled", "disabled");
        $('input[name="activo"]').removeAttr("checked");
    });

    $('#guardarBtn').on('click', function(event) {
        accionUsuario(event, 'POST');
    });

    $('#editarBtn').on('click', function(event) {
        accionUsuario(event, 'PUT');
    });

    $('#eliminarBtn').on('click', function(event) {
        accionUsuario(event, 'DELETE');
    });
});
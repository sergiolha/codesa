/**
 * 
 */
package com.sergioluisha.codesa;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Sergio Hernandez
 *
 */
@Entity
@Table(name = "ROL")
public class Rol {
	private @Id @Column(name = "ID_ROL") Long Id;
	private @Column(name = "NOMBRE", nullable = false, length = 150) String nombre;
	private @OneToMany(cascade = CascadeType.ALL, mappedBy = "rol", fetch = FetchType.EAGER) @JsonIgnoreProperties("rol") List<Usuario> usuarios = new ArrayList<Usuario>();

	public Rol() {
	}

	/**
	 * @param nombre
	 * @param usuarios
	 */
	public Rol(String nombre, List<Usuario> usuarios) {
		this.nombre = nombre;
		this.usuarios = usuarios;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.Id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the usuarios
	 */
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	/**
	 * @param usuarios the usuarios to set
	 */
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public void addUsuario(Usuario usuario) {
		addUsuario(usuario, true);
	}

	public void addUsuario(Usuario usuario, boolean set) {
		if (usuario != null) {
			if (getUsuarios().contains(usuario)) {
				getUsuarios().set(getUsuarios().indexOf(usuario), usuario);
			} else {
				getUsuarios().add(usuario);
			}
			if (set) {
				usuario.setRol(this, false);
			}
		}
	}

	public void removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setRol(null);
	}

	@Override
	public int hashCode() {
		return Objects.hash(Id, nombre, usuarios);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Rol))
			return false;
		Rol other = (Rol) obj;
		return Id == other.Id && Objects.equals(nombre, other.nombre) && Objects.equals(usuarios, other.usuarios);
	}

	@Override
	public String toString() {
		return "Rol [id=" + Id + ", nombre=" + nombre + ", usuarios=" + usuarios + "]";
	}

}

/**
 * 
 */
package com.sergioluisha.codesa;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sergio Hernandez
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:8181")
public class RolController {
	private final RolRepository rolRepository;
	private final RolModelAssembler assembler;
	
	/**
	 * @param rol_repository
	 * @param assembler
	 */
	public RolController(RolRepository rol_repository, RolModelAssembler assembler) {
		this.rolRepository = rol_repository;
		this.assembler = assembler;
	}
	
	@GetMapping("/roles")
	public CollectionModel<EntityModel<Rol>> all() {
		List<EntityModel<Rol>> roles = rolRepository.findAll().stream().map(assembler::toModel).collect(Collectors.toList());
		return CollectionModel.of(roles, linkTo(methodOn(RolController.class).all()).withSelfRel());
	}
	
	@GetMapping("/roles/{id}")
	public EntityModel<Rol> one(@PathVariable Long id) {
		Rol rol = rolRepository.findById(id).orElseThrow(() -> new RolNotFoundException(id));
		return assembler.toModel(rol);
	}
}

/**
 * 
 */
package com.sergioluisha.codesa;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 * @author Sergio Hernandez
 *
 */
@Component
public class RolModelAssembler implements RepresentationModelAssembler<Rol, EntityModel<Rol>> {

	@Override
	public EntityModel<Rol> toModel(Rol rol) {
		return EntityModel.of(rol, linkTo(methodOn(RolController.class).one(rol.getId())).withSelfRel());
	}

}

/**
 * 
 */
package com.sergioluisha.codesa;

/**
 * @author Sergio Hernandez
 *
 */
public class RolNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RolNotFoundException(Long id) {
		super("No existe rol con ID " + id);
	}
}

/**
 * 
 */
package com.sergioluisha.codesa;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Sergio Hernandez
 *
 */
public interface RolRepository extends JpaRepository<Rol, Long> {

}

/**
 * 
 */
package com.sergioluisha.codesa;

import java.util.Objects;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Sergio Hernandez
 *
 */
@Entity
@Table(name = "USUARIO")
public class Usuario {
	private @Id @Column(name = "ID_USUARIO") @GeneratedValue(generator = "seq_usuario") @SequenceGenerator(name = "seq_usuario", sequenceName = "SEQ_USUARIO", allocationSize = 1) Long Id;
	private @Column(name = "NOMBRE", nullable = false, length = 150) String nombre;
	private @Column(name = "ACTIVO", nullable = false) char activo;
	private @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER) @JsonIgnoreProperties("usuarios") @JoinColumn(name = "ID_ROL") Rol rol;
	private @Transient Long rol_id;
	
	public Usuario() {
	}

	/**
	 * @param nombre
	 * @param activo
	 */
	public Usuario(String nombre, char activo) {
		this.nombre = nombre;
		this.activo = activo;
	}

	/**
	 * @return the Id
	 */
	public Long getId() {
		return Id;
	}

	/**
	 * @param Id the Id to set
	 */
	public void setId(Long id) {
		this.Id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activo
	 */
	public char getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(char activo) {
		this.activo = activo;
	}

	/**
	 * @return the rol
	 */
	public Rol getRol() {
		return rol;
	}

	/**
	 * @param rol the rol to set
	 */
	public void setRol(Rol rol) {
		this.setRol(rol, true);
	}
	
	public void setRol(Rol rol, boolean add) {
		this.rol = rol;
		if (rol != null && add) {
			rol.addUsuario(this, false);
		}
	}

	/**
	 * @return the rol_id
	 */
	public Long getRol_id() {
		return rol_id;
	}

	/**
	 * @param rol_id the rol_id to set
	 */
	public void setRol_id(Long rol_id) {
		this.rol_id = rol_id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(activo, Id, nombre, rol);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Usuario))
			return false;
		Usuario other = (Usuario) obj;
		return activo == other.activo && Id == other.Id && Objects.equals(nombre, other.nombre)
				&& Objects.equals(rol, other.rol);
	}

	@Override
	public String toString() {
		return "Usuario [id=" + Id + ", nombre=" + nombre + ", activo=" + activo + ", rol=" + rol + "]";
	}
	
}

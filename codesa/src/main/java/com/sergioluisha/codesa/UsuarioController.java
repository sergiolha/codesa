/**
 * 
 */
package com.sergioluisha.codesa;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sergio Hernandez
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:8181")
public class UsuarioController {
	private final UsuarioRepository usuarioRepository;
	private final RolRepository rolRepository;
	private final UsuarioModelAssembler assembler;
	
	/**
	 * @param usuario_repository
	 * @param rol_repository
	 * @param assembler
	 */
	public UsuarioController(UsuarioRepository usuario_repository, RolRepository rol_repository, UsuarioModelAssembler assembler) {
		this.usuarioRepository = usuario_repository;
		this.rolRepository = rol_repository;
		this.assembler = assembler;
	}

	@GetMapping("/usuarios")
	public CollectionModel<EntityModel<Usuario>> all(@RequestParam(required=false, defaultValue="") String busqueda) {
		List<EntityModel<Usuario>> usuarios = Collections.<EntityModel<Usuario>>emptyList();
		List<Usuario> usuariosList = new ArrayList<Usuario>();
		usuariosList = busqueda.isBlank() ? usuarioRepository.findAll() : usuarioRepository.findByNombre(busqueda);
		usuariosList.stream().forEach((usuario) -> { usuario.setRol_id(usuario.getRol().getId()); });
		usuarios = usuariosList.stream().map(a -> busqueda.isBlank() ? assembler.toModel(a) : assembler.toModel(a, busqueda)).collect(Collectors.toList());
		return CollectionModel.of(usuarios, linkTo(methodOn(UsuarioController.class).all(busqueda)).withSelfRel());
	}
	
	@PostMapping("/usuarios")
	public ResponseEntity<?> newUsuario(@RequestBody Usuario newUsuario) {
		Optional<Rol> optionalRol = Optional.of(rolRepository.findById(newUsuario.getRol_id()).orElseThrow(() -> new RolNotFoundException(newUsuario.getRol_id())));
		Rol rol = optionalRol.get();
		rol.addUsuario(newUsuario);
		if (usuarioRepository.findByNombre(newUsuario.getNombre()).size() > 0) {
			throw new UsuarioNotUniqueException(newUsuario.getNombre());
		}
		EntityModel<Usuario> entityModel = assembler.toModel(usuarioRepository.save(newUsuario));
		return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
	}

	@GetMapping("/usuarios/{id}")
	public EntityModel<Usuario> one(@PathVariable Long id) {
		Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new UsuarioNotFoundException(id));
		return assembler.toModel(usuario);
	}
	
	@PutMapping("/usuarios/{id}")
	ResponseEntity<?> replaceUsuario(@RequestBody Usuario newUsuario, @PathVariable Long id) {
		Rol rol = rolRepository.findById(newUsuario.getRol_id()).orElseThrow(() -> new RolNotFoundException(newUsuario.getRol_id()));
		Usuario updatedUsuario = usuarioRepository.findById(id).map(usuario -> {
			List<Usuario> uniqueUsuario = usuarioRepository.findByNombre(newUsuario.getNombre());
			if (uniqueUsuario.size() > 0 && uniqueUsuario.get(0).getId() != usuario.getId()) {
				throw new UsuarioNotUniqueException(newUsuario.getNombre());
			}
			if (usuario.getRol_id() != newUsuario.getRol_id()) {
				usuario.getRol().removeUsuario(usuario);
				rol.addUsuario(usuario);
			}
			usuario.setNombre(newUsuario.getNombre());
			usuario.setRol_id(newUsuario.getRol_id());
			usuario.setActivo(newUsuario.getActivo());
			return usuarioRepository.save(usuario);
		}).orElseThrow(() -> {
			return new UsuarioNotFoundException(id);
		});
		EntityModel<Usuario> entityModel = assembler.toModel(updatedUsuario);
		return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
	}
	
	@DeleteMapping("/usuarios/{id}")
	ResponseEntity<?> deleteInstructor(@PathVariable Long id) {
		Optional<Usuario> usuarioOptional = Optional.of(usuarioRepository.findById(id).orElseThrow(() -> new UsuarioNotFoundException(id)));
		Usuario usuario = usuarioOptional.get();
		usuario.getRol().removeUsuario(usuario);
		usuarioRepository.deleteById(usuario.getId());
		return ResponseEntity.noContent().build();
	}

}

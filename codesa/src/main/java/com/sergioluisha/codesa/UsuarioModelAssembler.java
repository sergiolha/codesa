/**
 * 
 */
package com.sergioluisha.codesa;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

/**
 * @author Sergio Hernandez
 *
 */
@Component
public class UsuarioModelAssembler implements RepresentationModelAssembler<Usuario, EntityModel<Usuario>> {

	@Override
	public EntityModel<Usuario> toModel(Usuario usuario) {
		return EntityModel.of(usuario,
				linkTo(methodOn(UsuarioController.class).one(usuario.getId())).withSelfRel());
	}

	public EntityModel<Usuario> toModel(Usuario usuario, String busqueda) {
		return EntityModel.of(usuario,
				linkTo(methodOn(UsuarioController.class).one(usuario.getId())).withSelfRel(),
				linkTo(methodOn(UsuarioController.class).all(busqueda)).withRel("usuarios"));
	}

}

/**
 * 
 */
package com.sergioluisha.codesa;

/**
 * @author Sergio Hernandez
 *
 */
public class UsuarioNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNotFoundException(Long id) {
		super("No existe usuario con ID " + id);
	}
}

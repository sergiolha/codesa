/**
 * 
 */
package com.sergioluisha.codesa;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Sergio Hernandez
 *
 */
@ControllerAdvice
public class UsuarioNotUniqueAdvice {
	@ResponseBody
	@ExceptionHandler(UsuarioNotUniqueException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	String usuarioNotUniqueHandler(UsuarioNotUniqueException ex) {
		return "{\"error\":\"true\",\"description\":\"" + ex.getMessage() + "\"}";
	}
}

/**
 * 
 */
package com.sergioluisha.codesa;

/**
 * @author Sergio Hernandez
 *
 */
public class UsuarioNotUniqueException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNotUniqueException(String nombre) {
		super("Usuario repetido con nombre " + nombre);
	}
}

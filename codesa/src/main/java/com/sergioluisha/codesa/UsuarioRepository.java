/**
 * 
 */
package com.sergioluisha.codesa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Sergio Hernandez
 *
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	List<Usuario> findByNombre(String busqueda);
}
